use std::boxed::Box;
use std::error::Error;
use std::io;
use std::sync::mpsc;
use std::thread;
use std::time::{Duration, SystemTime};
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

use figlet_rs::FIGfont;
use tui::backend::TermionBackend;
use tui::layout::{Alignment, Constraint, Direction, Layout};
use tui::widgets::{Block, Borders, Paragraph, Text, Widget};
use tui::Terminal;

enum Event {
    Input(Key),
    Tick,
}

fn main() -> Result<(), Box<dyn Error>> {
    let (tx, rx) = mpsc::sync_channel::<Event>(0);
    let tick_tx = tx.clone();
    thread::spawn(move || -> Result<(), mpsc::SendError<Event>> {
        loop {
            tick_tx.send(Event::Tick)?;
            thread::sleep(Duration::from_millis(10));
        }
    });

    thread::spawn(move || -> Result<(), mpsc::SendError<Event>> {
        let stdin = io::stdin();
        for key in stdin.keys().filter_map(|x| x.ok()) {
            tx.send(Event::Input(key))?;
        }
        Ok(())
    });

    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    // TODO: Add input handling. See: https://github.com/fdehau/tui-rs/blob/master/examples/termion_demo.rs
    terminal.clear()?;
    terminal.hide_cursor()?;

    let mut start_time: Option<SystemTime> = None;
    let mut result: Option<Duration> = None;
    let mut active: bool = false;
    loop {
        terminal.draw(|mut f| {
            let size = f.size();
            let block = Block::default().borders(Borders::ALL);
            let font = FIGfont::standand().unwrap();

            let text = match active {
                true => format!(
                    "{:.2}",
                    start_time.unwrap().elapsed().unwrap().as_secs_f64()
                ),
                false => match result {
                    None => "--:--".to_string(),
                    Some(dur) => format!("{:.2}", dur.as_secs_f64()),
                },
            };

            let text = font.convert(text.as_str()).unwrap().to_string();

            let l = [Text::raw(text)];
            let p = Paragraph::new(l.iter())
                .block(block)
                .alignment(Alignment::Center);

            f.render_widget(p, size);
        })?;
        let event = rx.recv()?;
        match event {
            Event::Input(key) => match key {
                Key::Char('q') => {
                    break;
                }
                Key::Char(' ') => {
                    if let Some(start) = start_time {
                        if start.elapsed()? < Duration::from_millis(200) {}
                    }
                    active = !active;
                    match active {
                        // Just ended timing
                        false => result = Some(start_time.unwrap().elapsed()?),
                        // Started timing
                        true => start_time = Some(SystemTime::now()),
                    }
                }
                _ => {}
            },
            Event::Tick => {}
        };
    }
    Ok(())
}
